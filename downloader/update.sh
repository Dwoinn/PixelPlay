#! /usr/bin/env bash

cd /recalbox/share/roms/apps/downloader

rm refresh.sh
curl -s -o refresh.sh https://gitlab.com/Dwoinn/PixelPlay/raw/master/downloader/refresh.sh

DOWNLOADER_VERSION=$(cat version.info)
rm version.info

if [ $DOWNLOADER_VERSION = 0 ]
then
    # Downloader
    cd ..
    mkdir downloaded_images
    cd downloaded_images
    curl -s -o download.png https://gitlab.com/Dwoinn/PixelPlay/raw/master/downloader/downloaded_images/download.png
    curl -s -o update.png https://gitlab.com/Dwoinn/PixelPlay/raw/master/downloader/downloaded_images/update.png
    cd ..
    printf "<?xml version=\"1.0\"?>" > gamelist.xml
    printf "\n<gameList>" >> gamelist.xml
    printf "\n\t<game>" >> gamelist.xml
    printf "\n\t\t<path>./downloader</path>" >> gamelist.xml
    printf "\n\t\t<name>Telecharger des jeux</name>" >> gamelist.xml
    printf "\n\t\t<desc>Téléchargez les jeux que vous souhaitez parmis la liste disponible</desc>" >> gamelist.xml
    printf "\n\t\t<image>./downloaded_images/download.png</image>" >> gamelist.xml
    printf "\n\t\t<rating>1.0</rating>" >> gamelist.xml
    printf "\n\t\t<releasedate>20181002T000000</releasedate>" >> gamelist.xml
    printf "\n\t\t<developer>WINTER Donovan</developer>" >> gamelist.xml
    printf "\n\t\t<publisher>DwoinN</publisher>" >> gamelist.xml
    printf "\n\t\t<genre>Application</genre>" >> gamelist.xml
    printf "\n\t</game>" >> gamelist.xml
    printf "\n\t<game>" >> gamelist.xml
    printf "\n\t\t<path>./downloader/refresh.sh</path>" >> gamelist.xml
    printf "\n\t\t<name>#Mettre à jour</name>" >> gamelist.xml
    printf "\n\t\t<desc>Mettez à jour la liste des jeux</desc>" >> gamelist.xml
    printf "\n\t\t<image>./downloaded_images/update.png</image>" >> gamelist.xml
    #printf "\n\t\t<rating>1.0</rating>" >> gamelist.xml
   # printf "\n\t\t<releasedate>20181002T000000</releasedate>" >> gamelist.xml
  #  printf "\n\t\t<developer>WINTER Donovan</developer>" >> gamelist.xml
 #  printf "\n\t\t<publisher>DwoinN</publisher>" >> gamelist.xml
#    printf "\n\t\t<genre>Application</genre>" >> gamelist.xml
    printf "\n\t</game>" >> gamelist.xml
    printf "\n</gameList>" >> gamelist.xml
    
    # GameBoy Color
	content="\t<game>"
	content="$content\n\t\t<path>./downloader/gbc</path>"
	content="$content\n\t\t<name>Gameboy Color</name>"
	content="$content\n\t\t<image>/etc/emulationstation/themes/recalbox/gbc/data/console.svg</image>"
#	content="$content\n\t\t<rating>1.0</rating>"
#	content="$content\n\t\t<genre>Console</genre>"
	content="$content\n\t</game>"
    sed -i '$i\'"$content" "gamelist.xml"
    cd downloader
    mkdir gbc
    cd gbc
    
    #       Pokemon Or
    curl -s -o pokemon_or.sh https://gitlab.com/Dwoinn/PixelPlay/raw/master/downloader/gbc/pokemon_or.sh
    cd ../..
    curl -s -o downloaded_images/pokemon_or.png "https://www.screenscraper.fr/image.php?gameid=82619&media=mixrbv1&hd=0&region=fr&num=&version=&maxwidth=745&maxheight=745"
	content="\t<game>"
	content="$content\n\t\t<path>./downloader/gbc/pokemon_or.sh</path>"
	content="$content\n\t\t<name>Pokémon : Version Or</name>"
	content="$content\n\t\t<desc>Pokémon Version Or est un jeu de rôle sorti sur Gameboy. Dirigez un dresseur de Pokémons qui doit devenir le meilleur d'entre tous. Pour cela, capturez des créatures et faites-les évoluer pour qu'elles deviennent de plus en plus fortes afin de battre la ligue Pokémon. </desc>"
	content="$content\n\t\t<image>./downloaded_images/pokemon_or.png</image>"
	content="$content\n\t\t<rating>0.85</rating>"
	content="$content\n\t\t<releasedate>20010604T000000</releasedate>"
	content="$content\n\t\t<developer>Game Freak</developer>"
	content="$content\n\t\t<publisher>Nintendo</publisher>"
	content="$content\n\t\t<players>1</players>"
	content="$content\n\t</game>"
	sed -i '$i\'"$content" "gamelist.xml"
fi

cd downloader
rm update.sh

#/etc/init.d/S31emulationstation restart