#! /usr/bin/env bash

DOWNLOADER_VERSION=1
echo "Version actuelle : v$DOWNLOADER_VERSION"

olVer=$(curl -s https://gitlab.com/Dwoinn/PixelPlay/raw/master/downloader/version.info)
echo "Version diponible : $olVer"

if [ $olVer = "v$DOWNLOADER_VERSION" ]
then
	echo "Déjà à jour"
else
	echo "Téléchargement de la mise à jour..."
	cd /recalbox/share/roms/apps/downloader/
	curl -s -o update.sh https://gitlab.com/Dwoinn/PixelPlay/raw/master/downloader/update.sh
	echo "Lancement de la mise à jour..."
	echo $DOWNLOADER_VERSION > version.info
	sh update.sh
fi
